import pandas
import numpy
import random
from kmeans import kmeans
from matplotlib import pyplot
from matplotlib import colors

import argparse

def plot_unclustered(dataset):
	pyplot.scatter(dataset[:,0],dataset[:,1],c='black',label='unclustered data')
	pyplot.xlabel('Income')
	pyplot.ylabel('Number of transactions')
	pyplot.legend()
	pyplot.title('Plot of data points')
	pyplot.savefig("unclustered.png")

def plot_clustered(dataset, args):
	# Generate labels and colors randomly
	all_colors = random.sample([k for k,v in colors.cnames.items()], args.cluster_count+1)
	labels=['cluster'+str(i) for i in range(args.cluster_count)]

	# Plot every cluster
	for k in range(args.cluster_count):
		pyplot.scatter(output[k+1][:,0],output[k+1][:,1],c=all_colors[k],label=labels[k])

	# Plot the centroids
	pyplot.scatter(centroids[0,:],centroids[1,:],marker='^',s=200,c=all_colors[-1],label='Centroids')
	
	pyplot.xlabel('Income')
	pyplot.ylabel('Number of transactions')
	pyplot.legend()
	pyplot.savefig("clustered.png")

if __name__ == "__main__":
	# Parse the arguments
	parser = argparse.ArgumentParser(description='Use the K-Means algorithm for data clustering')
	parser.add_argument('iteration_count', metavar='n_iter', type=int,
						help='The number of iterations to run the K-Means algorithm')
	parser.add_argument('cluster_count', metavar='K', type=int,
						help='The number of iterations to run the K-Means algorithm')

	args = parser.parse_args()
	file_data=pandas.read_csv('Mall_Customers.csv')

	# Loads the 3rd and 4th column from the CSV file
	dataset = file_data.iloc[:, [3, 4]].values

	# Create a kmeans instance and run the algorithm
	kmeans = kmeans(dataset, args.cluster_count)
	output, centroids = kmeans.iterate(args.iteration_count)

	# Plot the graphs
	plot_unclustered(dataset)
	plot_clustered(dataset, args)

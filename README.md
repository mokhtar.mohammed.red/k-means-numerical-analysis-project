# K-Means Numerical Analysis Project
A simple implementation of the K-Means numerical algorithm on a test dataset in python from scratch.

## Table of Contents

## Usage
To run the algorithm for 100 iterations and 5 clusters, execute `python3 main.py 100`

## Algorithm
```mermaid
graph TD
  load_csv[Load all data from CSV file] -->load_dataset
  load_dataset[dataset = two columns from CSV file] -->|Call constructor in kmeans.py| sample_count
  sample_count[sample_count = number of rows in dataset] --> feature_count
  feature_count[feature_count = number of columns in dataset] --> set_i
  set_i[i = 0] --> for_centroids

  for_centroids{"If (i < arguments.K)"} -->|Yes| set_centroid
  for_centroids -->|No| current_iter

  set_centroid["centroids[i] = a random element from dataset"]-->ipp
  ipp[i++]-->for_centroids

  current_iter["current_iter = 0"] -->for_iterations
  for_iterations{"If (current_iter < arguments.n_iter)"} -->|Yes| set_i_1
  for_iterations-->|No|plot

  set_i_1["i = 0"]-->for_k_1
  for_k_1{"If (i < arguments.K)"}-->|Yes| set_j_1
  for_k_1-->|No| set_i_2
  set_j_1["j = 0"]-->for_d_1
  for_d_1{"If (j < dataset.length)"}-->|Yes| find_dist
  for_d_1-->|No|ipp1
  find_dist["euclidian_distance[i][j] = (dataset[j] - centroids[i])^2"]-->jpp1
  jpp1[j++]-->for_d_1
  ipp1[i++]-->for_k_1

  set_i_2["i = 0"]-->for_k_2
  for_k_2{"If (i < sample_count)"}-->|Yes|find_min_dist
  for_k_2-->|No|set_i_3
  find_min_dist["clusters[i] = the closest cluster to dataset[i]"]-->ipp2
  ipp2[i++]-->for_k_2

  set_i_3["i = 0"]-->for_k_3
  for_k_3{"If (i < sample_count)"}-->|Yes|put_clusters
  for_k_3-->|No|set_i_4
  put_clusters["output[clusters[i]].append(dataset[i])"]-->ipp3
  ipp3[i++]-->for_k_3


  set_i_4["i = 0"]-->for_k_4
  for_k_4{"If (i < arguments.K)"}-->|Yes|recenter_clusters
  for_k_4-->|No|cipp
  recenter_clusters["centroids[i] = average_of(clusters[i])"]-->ipp4
  ipp4[i++]-->for_k_4

  cipp["current_iter++"]-->for_iterations
  plot[Plot both graphs to files]-->End("end")
```

## Files
`kmeans.py`: Contains the main algorithm code. It can be called from any other python script.
`main.py`: Contains the code that loads data from the CSV file and passes it to the K-Means algorithm.
`Mall_Customers.csv`: Contains the test dataset. Used by the script for the demo.

## Output
The `main.py` script produces two files:
1. `unclustered.png`: Contains the unclustered data in the CSV file
2. `clustered.png`: Contains the data after clustering

![Image of unclustered data](unclustered.png "Image of unclustered data")
![Image of clustered data](clustered.png "Image of clustered data")
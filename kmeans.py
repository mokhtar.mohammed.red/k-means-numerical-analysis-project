import pandas
import numpy
import random

class kmeans:
	def __init__(self, dataset, K):
		# Save the given params
		self.dataset = dataset
		self.K = K

		self.sample_count = self.dataset.shape[0] # The number of rows in the CSV file
		self.feature_count = self.dataset.shape[1] # The number of selected columns in each row

		# Create a numpy array in the form of [ [ ... ], [ ... ] ]
		self.centroids = numpy.array([]).reshape(self.feature_count,0)
		for i in range(self.K):
			 # Choose a random element from the dataset and add it to the list of centroids
			rand = random.randint(0,self.sample_count-1)
			self.centroids = numpy.c_[self.centroids,self.dataset[rand]]

	def iterate(self, iteration_count):
		output = {}
		for i in range(iteration_count):
			# Create a numpy array contraining self.sample_count elements 
			euclidian_distance = numpy.array([]).reshape(self.sample_count,0)
			for k in range(self.K):
				# Calculate the distance between every point and every centroid
				tempDist = numpy.sum((self.dataset-self.centroids[:,k])**2,axis=1)
				euclidian_distance = numpy.c_[euclidian_distance,tempDist]

			# Select the centroid that every point belongs to
			clusters = numpy.argmin(euclidian_distance,axis=1)+1
			
			# Create an object that contains an empty numpy array for every centroid
			Y={}
			for k in range(self.K):
				Y[k+1] = numpy.array([]).reshape(2,0)

			# Put each point in Y inside of the cluster it belongs to 
			for i in range(self.sample_count):
				Y[clusters[i]] = numpy.c_[Y[clusters[i]],self.dataset[i]]

			# Transform Y into [[,], [,], [,], [,] ... ] format
			# instead of [ [ ... ], [ ... ] ]
			for k in range(self.K):
				Y[k+1] = Y[k+1].T

			# Recenter the centroid into the middle of its region
			for k in range(self.K):
				self.centroids[:,k] = numpy.mean(Y[k+1],axis=0)

			output = Y

		return output, self.centroids

